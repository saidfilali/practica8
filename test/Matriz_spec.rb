require 'Racional'
require 'Matriz'

describe Racional do

before (:each) do
        @uno_uno = Racional.new(1,2)
        @uno_dos = Racional.new(3,4)
        @dos_uno = Racional.new(5,6)
        @dos_dos = Racional.new(7,8)
        @a = Racional.new(11,8)
        @b = Racional.new(19,12)
        @c = Racional.new(19,12)
        @d = Racional.new(11,8)
        @e = Racional.new(7,8)
        @f = Racional.new(33,32)
        @g = Racional.new(55,48)
        @h = Racional.new(64,89)
        @muestra1 = Matriz.new(2, 2, @uno_uno, @uno_dos, @dos_uno, @dos_dos)
        @muestra2 = Matriz.new(2, 2, @dos_dos, @dos_uno, @uno_dos, @uno_uno)
        @muestra3 = Matriz.new(2, 2, @a, @b, @c, @d)
	@muestra4 = Matriz.new(2, 2, 1, 2, 3, 4)
	@muestra5 = Matriz.new(2, 2, 4, 3, 2, 1)
	@muestra6 = Matriz.new(2, 2, 5, 5, 5, 5)
	@muestra7 = Matriz.new(2, 2, 7, 10, 15, 22)
	@muestra8 = Matriz.new(2, 2, @e, @f, @g, @h)
	@muestra9 = Matriz.new(2, 2, @uno_uno, @uno_uno, @uno_uno, @uno_uno)
end

it "Se debe poder sumar dos matrices de enteros" do
        (@muestra4 + @muestra5).should == @muestra6
end

it "Se debe poder sumar dos matrices de racionales" do
        (@muestra1 + @muestra2).should == @muestra3
end

it "Se debe poder multiplicar una matriz de Enteros" do
        (@muestra4 * @muestra4).should == @muestra7
end

it "Se debe poder multiplicar dos matrices de Racionales" do
        (@muestra1 * @muestra1).should == @muestra8
end


end


